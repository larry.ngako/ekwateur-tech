# ekwateur-tech

## Test Technique Java Backend - Ekwateur

## Description

le projet a été realisé en langage java utilisant la version java17, ce projet utilise la version 2.7.10 de Spring boot.
Nous avons utilisé une architecture hexagonale afin de faire une meilleur séparation de la couche métier(l'hexagone) avec les autres couches.
De ce fait nous avons comme couche:
- Domaine qui contient la logique métier
- Infrastruscture qui contient la couche de persistance et les potentiels appels sortant au system
- la couche application qui est celle qui contient les webservice ou appel entrant au system
le deomain ne depend d'aucune couche ni framework et il communique avec couche pzr des ports 

## DATA base
Nous avons utilisé une base de donnée H2

- console: http://localhost:8080/h2-console/login.jsp
- user: sa 
- il n'y a pas de password

## Documentaion:

- Open API : http://localhost:8080/api-docs
- Swagger UI : http://localhost:8080/swagger-ui/index.html

## Test
test d'integration SpringBootTest
