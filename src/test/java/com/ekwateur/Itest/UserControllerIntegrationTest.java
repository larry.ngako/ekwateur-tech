package com.ekwateur.Itest;

import com.ekwateur.domain.api.CustomerPort;
import com.ekwateur.domain.data.*;
import com.ekwateur.infrastructure.dto.CustomerDto;
import com.ekwateur.infrastructure.dto.InvoiceDto;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.Assert;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerPort customerPort;

    @Test
    public void greetingShouldCreateParticularCustomer() throws Exception {
        ParticularCustomer particularCustomer = new ParticularCustomer();
        particularCustomer.setCustomerName("NGAKO");
        particularCustomer.setCustomerFirstName("larry");
        particularCustomer.setCivility(Civility.M);
        CustomerDto customerDto = new CustomerDto();
        customerDto.setCustomerType(CustomerType.PARTICULAR.name());
        customerDto.setCustomerId(1L);
        customerDto.setCustomerName("NGAKO");
        customerDto.setCustomerFirstName("larry");

        when(customerPort.createParticularCustomer(particularCustomer)).thenReturn(customerDto);
        mockMvc.perform(MockMvcRequestBuilders.post("/customer/particular")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"customerId\": 0,\n" +
                                "  \"customerReference\": \"\",\n" +
                                "  \"customerName\": \"Larry\",\n" +
                                "  \"customerFirstName\": \"NGAKO\",\n" +
                                "  \"civility\": \"M\",\n" +
                                "  \"customerType\": \"PARTICULAR\" }"))
                .andExpect(MockMvcResultMatchers.status().isCreated());

    }

    @Test
    public void greetingShouldCreateProrCustomer() throws Exception {
        ProffessionalCustomer proffessionalCustomer = new ProffessionalCustomer();
        proffessionalCustomer.setTurnOver(10000000000L);
        proffessionalCustomer.setLegalCompanyName("EKWATEUR");
        proffessionalCustomer.setSiretNumber("015151zcevve");

        CustomerDto customerDto = new CustomerDto();
        customerDto.setCustomerType(CustomerType.PRO.name());
        customerDto.setCustomerId(1L);
        customerDto.setLegalCompanyName("EKWATEUR");
        customerDto.setSiretNumber("015151zcevve");
        customerDto.setTurnOver(10000000000L);

        when(customerPort.createProffessionalCustomer(proffessionalCustomer)).thenReturn(customerDto);
        mockMvc.perform(MockMvcRequestBuilders.post("/customer/proffessional")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"customerId\": 0,\n" +
                                "  \"siretNumber\": \"string\",\n" +
                                "  \"legalCompanyName\": \"EKWATEUR\",\n" +
                                "  \"turnOver\": 1000000000,\n" +
                                "  \"customerType\": \"PRO\" }"))
                .andExpect(MockMvcResultMatchers.status().isCreated());

    }

    @Test
    public void greetingShouldCreateProInvoice() throws Exception {
        ProffessionalCustomer proffessionalCustomer = new ProffessionalCustomer();
        proffessionalCustomer.setTurnOver(10000000000L);
        proffessionalCustomer.setLegalCompanyName("EKWATEUR");
        proffessionalCustomer.setSiretNumber("015151xxxx");

        CustomerDto customerDto = new CustomerDto();
        customerDto.setCustomerType(CustomerType.PRO.name());
        customerDto.setCustomerId(1L);
        customerDto.setLegalCompanyName("EKWATEUR");
        customerDto.setSiretNumber("015151xxxx");
        customerDto.setTurnOver(10000000000L);
        customerDto.setCustomerReference("EKW00001234");

        when(customerPort.createProffessionalCustomer(proffessionalCustomer)).thenReturn(customerDto);
        mockMvc.perform(MockMvcRequestBuilders.post("/customer/proffessional")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"customerId\": 0,\n" +
                                "  \"siretNumber\": \"string\",\n" +
                                "  \"legalCompanyName\": \"EKWATEUR\",\n" +
                                "  \"turnOver\": 1000000000,\n" +
                                "  \"customerType\": \"PRO\" }"))
                .andExpect(MockMvcResultMatchers.status().isCreated());


        when(customerPort.createProffessionalCustomer(proffessionalCustomer)).thenReturn(customerDto);
        InvoiceHeader invoiceHeader = new InvoiceHeader();
        invoiceHeader.setYear(2023);
        invoiceHeader.setInvoiceReference("test001");
        invoiceHeader.setCustomerReference(customerDto.getCustomerReference());
        invoiceHeader.setMonth(04);
        Set<InvoiceLine> invoiceLines = new HashSet<>();
        invoiceLines.add(new InvoiceLine(EnergyType.ELECTRICITY, 1000));
        invoiceLines.add(new InvoiceLine(EnergyType.GAS, 300));
        invoiceHeader.setInvoiceLines(invoiceLines);

        mockMvc.perform(MockMvcRequestBuilders.post("/customer/"+customerDto.getCustomerReference()+"/invoice")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"invoiceId\": 0,\n" +
                                "  \"invoiceReference\": \"\",\n" +
                                "  \"totalAmount\": 0,\n" +
                                "  \"customerReference\": \"EKW00001234\",\n" +
                                "  \"yearInvoice\": 2023,\n" +
                                "  \"monthInvoice\": 4,\n" +
                                "  \"invoiceLines\": [\n" +
                                "    {\n" +
                                "      \"id\": 0,\n" +
                                "      \"energyType\": \"GAS\",\n" +
                                "      \"quantity\": 200,\n" +
                                "      \"amount\": 0\n" +
                                "    }\n" +
                                "  ]}"))
                .andExpect(MockMvcResultMatchers.status().isCreated());

    }
}
