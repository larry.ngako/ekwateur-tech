package com.ekwateur.domain.api;

import com.ekwateur.domain.data.InvoiceHeader;
import com.ekwateur.domain.data.ParticularCustomer;
import com.ekwateur.domain.data.ProffessionalCustomer;
import com.ekwateur.infrastructure.dto.CustomerDto;
import com.ekwateur.infrastructure.dto.InvoiceDto;

public interface CustomerPort {
    CustomerDto createParticularCustomer(ParticularCustomer particularCustomer);
    CustomerDto createProffessionalCustomer(ProffessionalCustomer proffessionalCustomer);

    InvoiceDto createInvoice(InvoiceHeader invoiceHeader);
}
