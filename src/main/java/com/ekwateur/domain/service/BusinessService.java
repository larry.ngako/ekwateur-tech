package com.ekwateur.domain.service;

import com.ekwateur.domain.api.CustomerPort;
import com.ekwateur.domain.data.*;
import com.ekwateur.domain.spi.CustomerPersistancePort;
import com.ekwateur.infrastructure.dto.CustomerDto;
import com.ekwateur.infrastructure.dto.InvoiceDto;

import java.math.BigDecimal;
import java.util.Optional;

public class BusinessService implements CustomerPort {

    private static final double PRICE_ELECTRICITY_FOR_PARTICULAR = 0.121;
    private static final double PRICE_GAS_FOR_PARTICULAR = 0.115;
    private static final double PRICE_ELECTRICITY_FOR_HIGTH_TURNOVER = 0.114;
    private static final double PRICE_GAS_FOR_HIGTH_TURNOVER = 0.111;
    private static final double PRICE_ELECTRICITY_FOR_LOW_TURNOVER = 0.118;
    private static final double PRICE_GAS_FOR_LOW_TURNOVER =0.113;
    private final  CustomerPersistancePort customerPersistancePort;

    public BusinessService(CustomerPersistancePort customerPersistancePort) {
        this.customerPersistancePort = customerPersistancePort;
    }

    @Override
    public CustomerDto createParticularCustomer(ParticularCustomer particularCustomer){
        return customerPersistancePort.createParticularCustomer(particularCustomer);
    }
    @Override
    public CustomerDto createProffessionalCustomer(ProffessionalCustomer proffessionalCustomer){
        return customerPersistancePort.createProffessionalCustomer(proffessionalCustomer);
    }

    @Override
    public InvoiceDto createInvoice(InvoiceHeader invoiceHeader) {
        CustomerDto customerDto = customerPersistancePort.findByCustomerReference(invoiceHeader.getCustomerReference());
        double priceElectricity = getPrixElectricite(customerDto);
        double priceGas = getPrixGaz(customerDto);
        double electricityConsumption = 0;
                Optional<InvoiceLine> electricityLine = invoiceHeader.getInvoiceLines().stream().filter(i -> i.getEnergyType().equals(EnergyType.ELECTRICITY)).findFirst();
        if(electricityLine.isPresent()){
            electricityConsumption = getElectricityConsumption(priceElectricity, electricityLine.get());
        }
        double gasConsumption = 0;
        Optional<InvoiceLine> gasLine = invoiceHeader.getInvoiceLines().stream().filter(i -> i.getEnergyType().equals(EnergyType.GAS)).findFirst();
        if(gasLine.isPresent()){
            electricityConsumption = getGasConsumption(priceGas, gasLine.get());
        }
        invoiceHeader.setTotalAmount(BigDecimal.valueOf(electricityConsumption + gasConsumption));
        for(InvoiceLine invoiceLine: invoiceHeader.getInvoiceLines()){
            if(invoiceLine.getEnergyType().equals(EnergyType.ELECTRICITY)){
                invoiceLine.setAmount(BigDecimal.valueOf(electricityConsumption));
            }else{
                invoiceLine.setAmount(BigDecimal.valueOf(gasConsumption));
            }
        }

        return customerPersistancePort.createInvoice(invoiceHeader);
    }

    private double getGasConsumption(double priceGas, InvoiceLine invoiceLine) {
        return priceGas * invoiceLine.getQuantity();
    }

    private double getElectricityConsumption(double priceElectricity, InvoiceLine invoiceLine) {
        return priceElectricity * invoiceLine.getQuantity();
    }


    private double getPrixElectricite(CustomerDto customerDto) {
        if (customerDto.getCustomerType().equals("PARTICULAR")) {
            return PRICE_ELECTRICITY_FOR_PARTICULAR;
        } else if (customerDto.getCustomerType().equals("PRO")) {
            double ca = customerDto.getTurnOver();
            if (ca > 1000000) {
                return PRICE_ELECTRICITY_FOR_HIGTH_TURNOVER;
            } else {
                return PRICE_ELECTRICITY_FOR_LOW_TURNOVER;
            }
        } else {
            throw new IllegalArgumentException("unknow customer");
        }
    }

    private double getPrixGaz(CustomerDto customerDto) {
        if (customerDto.getCustomerType().equals("PARTICULAR")) {
            return PRICE_GAS_FOR_PARTICULAR;
        } else if (customerDto.getCustomerType().equals("PRO")) {
            double ca =  customerDto.getTurnOver();
            if (ca > 1000000) {
                return PRICE_GAS_FOR_HIGTH_TURNOVER;
            } else {
                return PRICE_GAS_FOR_LOW_TURNOVER;
            }
        } else {
            throw new IllegalArgumentException("unknow customer");
        }
    }




}
