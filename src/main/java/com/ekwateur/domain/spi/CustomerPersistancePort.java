package com.ekwateur.domain.spi;

import com.ekwateur.domain.data.InvoiceHeader;
import com.ekwateur.domain.data.ParticularCustomer;
import com.ekwateur.domain.data.ProffessionalCustomer;
import com.ekwateur.infrastructure.dto.CustomerDto;
import com.ekwateur.infrastructure.dto.InvoiceDto;

public interface CustomerPersistancePort {

    CustomerDto createParticularCustomer(ParticularCustomer particularCustomer);
    CustomerDto createProffessionalCustomer(ProffessionalCustomer proffessionalCustomer);
    CustomerDto findByCustomerReference(String customerreference);

    InvoiceDto createInvoice(InvoiceHeader invoiceHeader);
}
