package com.ekwateur.domain.data;

import java.math.BigDecimal;

public class PriceCustomer {
    private BigDecimal priceElectricityForParticular;
    private BigDecimal priceGasForParticular;
    private BigDecimal priceElectricityForHigthTurnover;
    private BigDecimal priceGasForHigthTurnover;
    private BigDecimal priceElectricityForLowTurnover;
    private BigDecimal priceGasForLowTurnover;
}
