package com.ekwateur.domain.data;

import java.util.Arrays;
import java.util.Optional;

public enum EnergyType {
    ELECTRICITY,
    GAS;

    public static EnergyType map(String value) {

        Optional<EnergyType> mapvalue = Arrays.stream(EnergyType.values())
                .filter(v -> v.name().equals(value))
                .findFirst();
        return mapvalue.get();
    }
}
