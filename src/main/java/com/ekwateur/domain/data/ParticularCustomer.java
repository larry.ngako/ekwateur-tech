package com.ekwateur.domain.data;

public class ParticularCustomer extends CommonCustomer{
    private String customerName;
    private String customerFirstName;
    private Civility civility;
    public ParticularCustomer() {
        super(CustomerType.PARTICULAR);
    }

    public ParticularCustomer(String customerName, String customerFirstName, Civility civility) {
        super(CustomerType.PARTICULAR);
        this.customerName = customerName;
        this.customerFirstName = customerFirstName;
        this.civility = civility;
    }
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public Civility getCivility() {
        return civility;
    }

    public void setCivility(Civility civility) {
        this.civility = civility;
    }
}
