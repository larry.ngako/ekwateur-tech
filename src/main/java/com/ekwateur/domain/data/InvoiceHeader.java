package com.ekwateur.domain.data;

import java.math.BigDecimal;
import java.util.Set;

public class InvoiceHeader {
    private Long invoiceId;
    private String invoiceReference;
    private int year;
    private int month;
    private BigDecimal totalAmount;
    private String customerReference;

    private Set<InvoiceLine> invoiceLines;

    public InvoiceHeader() {
    }

    public InvoiceHeader(Long invoiceId, String invoiceReference, BigDecimal totalAmount,
                         String customerReference, int month, int year) {
        this.invoiceId = invoiceId;
        this.invoiceReference = invoiceReference;
        this.totalAmount = totalAmount;
        this.customerReference = customerReference;
        this.month = month;
        this.year = year;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceReference() {
        return invoiceReference;
    }

    public void setInvoiceReference(String invoiceReference) {
        this.invoiceReference = invoiceReference;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public Set<InvoiceLine> getInvoiceLines() {
        return invoiceLines;
    }

    public void setInvoiceLines(Set<InvoiceLine> invoiceLines) {
        this.invoiceLines = invoiceLines;
    }

}
