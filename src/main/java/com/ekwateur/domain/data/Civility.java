package com.ekwateur.domain.data;

import java.util.Arrays;
import java.util.Optional;

public enum Civility {
    M ,
    Mme;

    public static Civility map(String value) {

        Optional<Civility> mapvalue = Arrays.stream(Civility.values())
                .filter(v -> v.name().equals(value))
                .findFirst();
        return mapvalue.get();
    }
}
