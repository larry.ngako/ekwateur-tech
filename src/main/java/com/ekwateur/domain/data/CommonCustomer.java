package com.ekwateur.domain.data;

import java.util.Objects;
import java.util.Random;

public abstract class CommonCustomer {

    private Long customerId;
    private String customerReference;
    private CustomerType customerType;

    public CommonCustomer(CustomerType customerType) {
        this.customerType = customerType;
        Random rand = new Random();
        this.customerReference = "EKW" + String.format("%08d", rand.nextInt(100000000));
    }

    public CommonCustomer(Long customerId, String customerReference, CustomerType customerType) {
        this.customerId = customerId;
        this.customerReference = customerReference;
        this.customerType = customerType;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public CustomerType getCustomerType() {
        return customerType;
    }

    public void setCustomerType(CustomerType customerType) {
        this.customerType = customerType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommonCustomer)) return false;
        CommonCustomer that = (CommonCustomer) o;
        return customerReference.equals(that.customerReference);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerReference);
    }
}
