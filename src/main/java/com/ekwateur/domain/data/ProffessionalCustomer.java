package com.ekwateur.domain.data;

public class ProffessionalCustomer extends CommonCustomer{

    private String siretNumber;
    private String legalCompanyName;
    private Long turnOver;
    public ProffessionalCustomer() {
        super(CustomerType.PRO);
    }

    public ProffessionalCustomer(String siretNumber, String legalCompanyName, Long turnOver) {
        super(CustomerType.PRO);
        this.siretNumber = siretNumber;
        this.legalCompanyName = legalCompanyName;
        this.turnOver = turnOver;
    }

    public String getSiretNumber() {
        return siretNumber;
    }

    public void setSiretNumber(String siretNumber) {
        this.siretNumber = siretNumber;
    }

    public String getLegalCompanyName() {
        return legalCompanyName;
    }

    public void setLegalCompanyName(String legalCompanyName) {
        this.legalCompanyName = legalCompanyName;
    }

    public Long getTurnOver() {
        return turnOver;
    }

    public void setTurnOver(Long turnOver) {
        this.turnOver = turnOver;
    }
}
