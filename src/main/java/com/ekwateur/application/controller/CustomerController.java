package com.ekwateur.application.controller;

import com.ekwateur.application.mapper.CustomerMapper;
import com.ekwateur.application.mapper.InvoiceMapper;
import com.ekwateur.application.modelview.InvoiceModelView;
import com.ekwateur.application.modelview.ParticularCustomerModelView;
import com.ekwateur.application.modelview.ProffessionalCustomerModelView;
import com.ekwateur.domain.api.CustomerPort;
import com.ekwateur.domain.data.InvoiceHeader;
import com.ekwateur.domain.data.ParticularCustomer;
import com.ekwateur.domain.data.ProffessionalCustomer;
import com.ekwateur.infrastructure.dto.CustomerDto;
import com.ekwateur.infrastructure.dto.InvoiceDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerPort customerPort;

    public CustomerController(CustomerPort customerPort) {
        this.customerPort = customerPort;
    }

    @PostMapping("/particular")
    public ResponseEntity<CustomerDto> createParticularCustomer(@RequestBody ParticularCustomerModelView particularCustomerModelView) {
        ParticularCustomer particularCustomer = CustomerMapper.toParticularCustomer(particularCustomerModelView);
        CustomerDto customerDto = customerPort.createParticularCustomer(particularCustomer);
        return new ResponseEntity<>(customerDto, HttpStatus.CREATED);
    }
    @PostMapping("/proffessional")
    public ResponseEntity<CustomerDto> createProffessional(@RequestBody ProffessionalCustomerModelView proffessionalCustomerModelView) {
        ProffessionalCustomer proffessionalCustomer = CustomerMapper.toProffessionalCustomer(proffessionalCustomerModelView);
        CustomerDto customerDto = customerPort.createProffessionalCustomer(proffessionalCustomer);
        return new ResponseEntity<>(customerDto, HttpStatus.CREATED);
    }

    @PostMapping("/{refCustomer}/invoice")
    public ResponseEntity<InvoiceDto> createInvoiceCustomer(@RequestBody InvoiceModelView invoiceModelView) {
        InvoiceHeader invoiceHeader = InvoiceMapper.toInvoiceHeader(invoiceModelView);
        InvoiceDto invoiceDto = customerPort.createInvoice(invoiceHeader);
        return new ResponseEntity<>(invoiceDto, HttpStatus.CREATED);
    }
}
