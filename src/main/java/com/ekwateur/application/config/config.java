package com.ekwateur.application.config;

import com.ekwateur.domain.api.CustomerPort;
import com.ekwateur.domain.service.BusinessService;
import com.ekwateur.domain.spi.CustomerPersistancePort;
import com.ekwateur.infrastructure.adapter.CustomerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class config {


    @Bean
    public CustomerPersistancePort customerPersistancePort(){
        return new CustomerAdapter();
    }
    @Bean
    public CustomerPort customerPort(){
        return new BusinessService(customerPersistancePort());
    }
}
