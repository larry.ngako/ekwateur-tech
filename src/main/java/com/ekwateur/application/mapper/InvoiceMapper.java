package com.ekwateur.application.mapper;

import com.ekwateur.application.modelview.InvoiceModelView;
import com.ekwateur.application.modelview.ProffessionalCustomerModelView;
import com.ekwateur.domain.data.*;

import java.util.stream.Collectors;

public interface InvoiceMapper {
    static InvoiceHeader toInvoiceHeader(InvoiceModelView invoiceModelView){
        InvoiceHeader invoiceHeader = new InvoiceHeader();
        invoiceHeader.setInvoiceReference(invoiceModelView.getInvoiceReference());
        invoiceHeader.setCustomerReference(invoiceModelView.getCustomerReference());
        invoiceHeader.setMonth(invoiceModelView.getMonthInvoice());
        invoiceHeader.setYear(invoiceModelView.getYearInvoice());
        invoiceHeader.setTotalAmount(invoiceModelView.getTotalAmount());
        invoiceHeader.setInvoiceLines(
                invoiceModelView.getInvoiceLines().stream()
                                                   .map(i -> new InvoiceLine(
                                                           EnergyType.map(i.getEnergyType()),
                                                           i.getQuantity(),
                                                           i.getAmount()))
                                                    .collect(Collectors.toSet())
        );
        return invoiceHeader;
    }
    static ProffessionalCustomer toProffessionalCustomer(ProffessionalCustomerModelView proffessionalCustomerModelView){
        ProffessionalCustomer proffessionalCustomer = new ProffessionalCustomer();
        proffessionalCustomer.setCustomerId(proffessionalCustomerModelView.getCustomerId());
        proffessionalCustomer.setSiretNumber (proffessionalCustomerModelView.getSiretNumber());
        proffessionalCustomer.setTurnOver(proffessionalCustomerModelView.getTurnOver());
        proffessionalCustomer.setLegalCompanyName(proffessionalCustomerModelView.getLegalCompanyName());
        return proffessionalCustomer;
    }
}
