package com.ekwateur.application.mapper;

import com.ekwateur.application.modelview.ParticularCustomerModelView;
import com.ekwateur.application.modelview.ProffessionalCustomerModelView;
import com.ekwateur.domain.data.Civility;
import com.ekwateur.domain.data.ParticularCustomer;
import com.ekwateur.domain.data.ProffessionalCustomer;

public interface CustomerMapper {
    static ParticularCustomer toParticularCustomer(ParticularCustomerModelView particularCustomerModelView){
        ParticularCustomer particularCustomer = new ParticularCustomer();
        particularCustomer.setCustomerId(particularCustomerModelView.getCustomerId());
        particularCustomer.setCustomerName(particularCustomerModelView.getCustomerName());
        particularCustomer.setCivility(Civility.map(particularCustomerModelView.getCivility()));
        return particularCustomer;
    }
    static ProffessionalCustomer toProffessionalCustomer(ProffessionalCustomerModelView proffessionalCustomerModelView){
        ProffessionalCustomer proffessionalCustomer = new ProffessionalCustomer();
        proffessionalCustomer.setCustomerId(proffessionalCustomerModelView.getCustomerId());
        proffessionalCustomer.setSiretNumber (proffessionalCustomerModelView.getSiretNumber());
        proffessionalCustomer.setTurnOver(proffessionalCustomerModelView.getTurnOver());
        proffessionalCustomer.setLegalCompanyName(proffessionalCustomerModelView.getLegalCompanyName());
        return proffessionalCustomer;
    }
}
