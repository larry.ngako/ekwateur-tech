package com.ekwateur.application.modelview;

import lombok.*;

import java.io.Serializable;


public class ProffessionalCustomerModelView implements Serializable {
    private  Long customerId;
    private String customerReference;
    private String siretNumber;
    private String legalCompanyName;
    private Long turnOver;

    public ProffessionalCustomerModelView(Long customerId, String customerReference, String siretNumber, String legalCompanyName, Long turnOver) {
        this.customerId = customerId;
        this.customerReference = customerReference;
        this.siretNumber = siretNumber;
        this.legalCompanyName = legalCompanyName;
        this.turnOver = turnOver;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getSiretNumber() {
        return siretNumber;
    }

    public void setSiretNumber(String siretNumber) {
        this.siretNumber = siretNumber;
    }

    public String getLegalCompanyName() {
        return legalCompanyName;
    }

    public void setLegalCompanyName(String legalCompanyName) {
        this.legalCompanyName = legalCompanyName;
    }

    public Long getTurnOver() {
        return turnOver;
    }

    public void setTurnOver(Long turnOver) {
        this.turnOver = turnOver;
    }
}
