package com.ekwateur.application.modelview;

import com.ekwateur.domain.data.InvoiceHeader;

import java.io.Serializable;
import java.math.BigDecimal;


public class InvoiceLineModelView implements Serializable {

    private Long id;
    private String energyType;

    private InvoiceHeader invoiceHeader;
    private int quantity;
    private BigDecimal amount;

    public InvoiceLineModelView(Long id, String energyType, InvoiceHeader invoiceHeader, int quantity, BigDecimal amount) {
        this.id = id;
        this.energyType = energyType;
        this.invoiceHeader = invoiceHeader;
        this.quantity = quantity;
        this.amount = amount;
    }

    public InvoiceLineModelView() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEnergyType() {
        return energyType;
    }

    public void setEnergyType(String energyType) {
        this.energyType = energyType;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
