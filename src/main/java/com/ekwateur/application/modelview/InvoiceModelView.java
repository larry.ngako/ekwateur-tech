package com.ekwateur.application.modelview;



import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

public class InvoiceModelView implements Serializable {
    private Long invoiceId;
    private String invoiceReference;

    private BigDecimal totalAmount;
    private String customerReference;
    private int yearInvoice;
    private int monthInvoice;
    private Set<InvoiceLineModelView> invoiceLines;

    public InvoiceModelView() {
    }

    public InvoiceModelView(Long invoiceId, String invoiceReference, BigDecimal totalAmount, String customerReference,
                            int yearInvoice, int monthInvoice) {
        this.invoiceId = invoiceId;
        this.invoiceReference = invoiceReference;
        this.totalAmount = totalAmount;
        this.customerReference = customerReference;
        this.yearInvoice = yearInvoice;
        this.monthInvoice = monthInvoice;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceReference() {
        return invoiceReference;
    }

    public void setInvoiceReference(String invoiceReference) {
        this.invoiceReference = invoiceReference;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public int getYearInvoice() {
        return yearInvoice;
    }

    public void setYearInvoice(int yearInvoice) {
        this.yearInvoice = yearInvoice;
    }

    public int getMonthInvoice() {
        return monthInvoice;
    }

    public void setMonthInvoice(int monthInvoice) {
        this.monthInvoice = monthInvoice;
    }

    public Set<InvoiceLineModelView> getInvoiceLines() {
        return invoiceLines;
    }

    public void setInvoiceLines(Set<InvoiceLineModelView> invoiceLines) {
        this.invoiceLines = invoiceLines;
    }


}
