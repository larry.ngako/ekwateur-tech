package com.ekwateur.infrastructure.repository;

import com.ekwateur.infrastructure.entity.InvoiceLineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceLineRepository extends JpaRepository<InvoiceLineEntity, Long> {
}
