package com.ekwateur.infrastructure.repository;

import com.ekwateur.infrastructure.entity.InvoiceHeaderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends JpaRepository<InvoiceHeaderEntity, Long> {
}
