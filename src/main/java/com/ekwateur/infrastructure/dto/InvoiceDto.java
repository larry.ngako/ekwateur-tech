package com.ekwateur.infrastructure.dto;



import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

public class InvoiceDto {
    private Long invoiceId;
    private String invoiceReference;

    private BigDecimal totalAmount;
    private String customerReference;

    private Set<InvoiceLineDto> invoiceLines;
    private int year;
    private int month;

    public InvoiceDto(Long invoiceId, String invoiceReference, BigDecimal totalAmount, String customerReference,
                      int year, int month) {
        this.invoiceId = invoiceId;
        this.invoiceReference = invoiceReference;
        this.totalAmount = totalAmount;
        this.customerReference = customerReference;
        this.year = year;
        this.month = month;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceReference() {
        return invoiceReference;
    }

    public void setInvoiceReference(String invoiceReference) {
        this.invoiceReference = invoiceReference;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public Set<InvoiceLineDto> getInvoiceLines() {
        return invoiceLines;
    }

    public void setInvoiceLines(Set<InvoiceLineDto> invoiceLines) {
        this.invoiceLines = invoiceLines;
    }
}
