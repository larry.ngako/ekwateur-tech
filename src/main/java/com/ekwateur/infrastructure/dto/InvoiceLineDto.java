package com.ekwateur.infrastructure.dto;

import com.ekwateur.domain.data.EnergyType;
import com.ekwateur.domain.data.InvoiceHeader;

import java.math.BigDecimal;


public class InvoiceLineDto {

    private Long id;
    private EnergyType energyType;

    private InvoiceHeader invoiceHeader;
    private int quantity;
    private BigDecimal amount;

    public InvoiceLineDto(Long id, EnergyType energyType, int quantity, BigDecimal amount) {
        this.id = id;
        this.energyType = energyType;
        this.quantity = quantity;
        this.amount = amount;
    }
    public InvoiceLineDto(EnergyType energyType, int quantity, BigDecimal amount) {
        this.energyType = energyType;
        this.quantity = quantity;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public void setEnergyType(EnergyType energyType) {
        this.energyType = energyType;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
