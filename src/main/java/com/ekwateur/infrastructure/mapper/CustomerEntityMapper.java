package com.ekwateur.infrastructure.mapper;

import com.ekwateur.domain.data.Civility;
import com.ekwateur.domain.data.CustomerType;
import com.ekwateur.domain.data.ParticularCustomer;
import com.ekwateur.domain.data.ProffessionalCustomer;
import com.ekwateur.infrastructure.dto.CustomerDto;
import com.ekwateur.infrastructure.entity.CustomerEntity;

public interface CustomerEntityMapper {
    static CustomerEntity toParticularCustomerEntity(ParticularCustomer particularCustomer){
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setCustomerName(particularCustomer.getCustomerName());
        customerEntity.setCivility(Civility.map(particularCustomer.getCivility().name()));
        customerEntity.setCustomerReference(particularCustomer.getCustomerReference());
        customerEntity.setCustomerFirstname(particularCustomer.getCustomerFirstName());
        customerEntity.setCustomerType(CustomerType.PARTICULAR);

        return customerEntity;
    }
    static CustomerEntity toProffessionalCustomerEntity(ProffessionalCustomer proffessionalCustomer){
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setLegalCompanyName(proffessionalCustomer.getLegalCompanyName());
        customerEntity.setSiretNumber(proffessionalCustomer.getSiretNumber());
        customerEntity.setTurnOver(proffessionalCustomer.getTurnOver());
        customerEntity.setCustomerReference(proffessionalCustomer.getCustomerReference());
        customerEntity.setCustomerType(CustomerType.PRO);
        return customerEntity;
    }

    static CustomerDto toParticularCustomerDto(CustomerEntity customerEntity){

        CustomerDto customerDto = new CustomerDto();
        customerDto.setCustomerId(customerEntity.getCustomerId());
        customerDto.setCustomerName(customerEntity.getCustomerName());
        customerDto.setCivility(customerEntity.getCivility().name());
        customerDto.setCustomerType(CustomerType.PARTICULAR.name());
        customerDto.setCustomerReference(customerEntity.getCustomerReference());
        return customerDto;
    }
    static CustomerDto toProffessionalCustomerDto(CustomerEntity customerEntity){

        CustomerDto customerDto = new CustomerDto();
        customerDto.setCustomerId(customerEntity.getCustomerId());
        customerDto.setLegalCompanyName(customerEntity.getLegalCompanyName());
        customerDto.setSiretNumber(customerEntity.getSiretNumber());
        customerDto.setTurnOver(customerEntity.getTurnOver());
        customerDto.setCustomerReference(customerEntity.getCustomerReference());
        customerDto.setCustomerType(CustomerType.PRO.name());
        return customerDto;
    }
}
