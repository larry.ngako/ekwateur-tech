package com.ekwateur.infrastructure.mapper;

import com.ekwateur.domain.data.*;
import com.ekwateur.infrastructure.dto.InvoiceDto;
import com.ekwateur.infrastructure.dto.InvoiceLineDto;
import com.ekwateur.infrastructure.entity.InvoiceHeaderEntity;
import com.ekwateur.infrastructure.entity.InvoiceLineEntity;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public interface InvoiceEntityMapper {
    static InvoiceHeaderEntity toInvoiceHeaderEntity(InvoiceHeader invoiceHeader){
        InvoiceHeaderEntity invoiceHeaderEntity = new InvoiceHeaderEntity();
        invoiceHeaderEntity.setCustomerReference(invoiceHeader.getCustomerReference());
        invoiceHeaderEntity.setInvoiceReference(invoiceHeader.getInvoiceReference());
        invoiceHeaderEntity.setMonthInvoice(invoiceHeader.getMonth());
        invoiceHeaderEntity.setYearInvoice(invoiceHeader.getYear());
        invoiceHeaderEntity.setTotalAmount(invoiceHeader.getTotalAmount());
        return invoiceHeaderEntity;
    }

    static InvoiceDto toInvoiceDto(InvoiceHeader invoiceHeade, Set<InvoiceLineEntity> invoiceLineEntities){
        InvoiceDto invoiceDto = new InvoiceDto(invoiceHeade.getInvoiceId(), invoiceHeade.getInvoiceReference(),
                invoiceHeade.getTotalAmount(), invoiceHeade.getCustomerReference(),invoiceHeade.getYear(), invoiceHeade.getMonth());
        Set<InvoiceLineDto> invoiceLines = new HashSet<>();
        for(InvoiceLineEntity i: invoiceLineEntities){
            invoiceLines.add(new InvoiceLineDto(i.getId(), i.getEnergyType(), i.getQuantity(), i.getAmount()));
        }
        invoiceDto.setInvoiceLines(invoiceLines);
        return invoiceDto;
    }

    static InvoiceLineEntity toInvoiceLineEntity(InvoiceLine invoiceLine, InvoiceHeaderEntity invoiceHeader){

        InvoiceLineEntity invoiceLineEntity = new InvoiceLineEntity(invoiceLine.getId(),
                invoiceLine.getEnergyType(), invoiceLine.getQuantity(), invoiceLine.getAmount(), invoiceHeader.getInvoiceReference());

        return invoiceLineEntity;
    }
}
