package com.ekwateur.infrastructure.adapter;

import com.ekwateur.domain.data.*;
import com.ekwateur.domain.spi.CustomerPersistancePort;
import com.ekwateur.infrastructure.dto.CustomerDto;
import com.ekwateur.infrastructure.dto.InvoiceDto;
import com.ekwateur.infrastructure.entity.CustomerEntity;
import com.ekwateur.infrastructure.entity.InvoiceHeaderEntity;
import com.ekwateur.infrastructure.entity.InvoiceLineEntity;
import com.ekwateur.infrastructure.mapper.CustomerEntityMapper;
import com.ekwateur.infrastructure.mapper.InvoiceEntityMapper;
import com.ekwateur.infrastructure.repository.CustomerRepository;
import com.ekwateur.infrastructure.repository.InvoiceLineRepository;
import com.ekwateur.infrastructure.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class CustomerAdapter implements CustomerPersistancePort {
    @Autowired
    private  CustomerRepository customerRepository;
    @Autowired
    private  InvoiceRepository invoiceRepository;
    @Autowired
    private  InvoiceLineRepository invoiceLineRepository;

    public CustomerAdapter(){
    }

    @Override
    public CustomerDto createParticularCustomer(ParticularCustomer particularCustomer) {
        CustomerEntity customerEntity = CustomerEntityMapper.toParticularCustomerEntity(particularCustomer);
        return CustomerEntityMapper.toParticularCustomerDto(customerRepository.save(customerEntity));
    }

    @Override
    public CustomerDto createProffessionalCustomer(ProffessionalCustomer proffessionalCustomer) {
        CustomerEntity customerEntity = CustomerEntityMapper.toProffessionalCustomerEntity(proffessionalCustomer);
        return CustomerEntityMapper.toProffessionalCustomerDto(customerRepository.save(customerEntity));
    }
    @Override
    public CustomerDto findByCustomerReference(String customerreference){
        Optional<CustomerEntity> customerEntity = customerRepository.findByCustomerReference(customerreference);
        if (customerEntity.isPresent() && customerEntity.get().getCustomerType().equals(CustomerType.PARTICULAR)){
            return CustomerEntityMapper.toParticularCustomerDto(customerEntity.get());
        }else if (customerEntity.isPresent() && customerEntity.get().getCustomerType().equals(CustomerType.PRO)){
            return CustomerEntityMapper.toProffessionalCustomerDto(customerEntity.get());
        }else {
            throw new IllegalArgumentException("unknow customer");
        }

    }

    @Override
    public InvoiceDto createInvoice(InvoiceHeader invoiceHeader) {
        InvoiceHeaderEntity invoiceHeaderEntity = InvoiceEntityMapper.toInvoiceHeaderEntity(invoiceHeader);
        invoiceHeaderEntity = invoiceRepository.save(invoiceHeaderEntity);
        Set<InvoiceLineEntity> invoiceLineEntitySet = new HashSet<>();
        for (InvoiceLine invoiceLine: invoiceHeader.getInvoiceLines()){
            invoiceLineEntitySet.add(invoiceLineRepository.save(InvoiceEntityMapper.toInvoiceLineEntity(invoiceLine,
                    invoiceHeaderEntity)));
        }
        return InvoiceEntityMapper.toInvoiceDto(invoiceHeader, invoiceLineEntitySet);
    }
}
