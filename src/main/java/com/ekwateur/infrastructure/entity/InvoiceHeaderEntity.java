package com.ekwateur.infrastructure.entity;

import javax.persistence.*;
import java.math.BigDecimal;


@Entity(name = "invoice_header")
public class InvoiceHeaderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long invoiceId;
    @Column( nullable = false, unique = true)
    private String invoiceReference;
    private BigDecimal totalAmount;
    private String customerReference;
    private Integer yearInvoice;
    private Integer monthInvoice;

    public InvoiceHeaderEntity() {
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceReference() {
        return invoiceReference;
    }

    public void setInvoiceReference(String invoiceReference) {
        this.invoiceReference = invoiceReference;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public Integer getYearInvoice() {
        return yearInvoice;
    }

    public void setYearInvoice(Integer yearInvoice) {
        this.yearInvoice = yearInvoice;
    }

    public Integer getMonthInvoice() {
        return monthInvoice;
    }

    public void setMonthInvoice(Integer monthInvoice) {
        this.monthInvoice = monthInvoice;
    }
}
