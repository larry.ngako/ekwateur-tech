package com.ekwateur.infrastructure.entity;


import com.ekwateur.domain.data.Civility;
import com.ekwateur.domain.data.CustomerType;

import javax.persistence.*;


@Entity(name = "customer")
@Table(indexes = {
        @Index(columnList = "customerReference")
})
public class CustomerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long customerId;
    private CustomerType customerType;
    @Column(nullable = false, length = 11)
    private String customerReference;
    private String customerName;
    private String customerFirstname;
    private Civility civility;
    private String siretNumber;
    private String legalCompanyName;
    private Long turnOver;

    public CustomerEntity() {
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public CustomerType getCustomerType() {
        return customerType;
    }

    public void setCustomerType(CustomerType customerType) {
        this.customerType = customerType;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerFirstname() {
        return customerFirstname;
    }

    public void setCustomerFirstname(String customerFirstname) {
        this.customerFirstname = customerFirstname;
    }

    public Civility getCivility() {
        return civility;
    }

    public void setCivility(Civility civility) {
        this.civility = civility;
    }

    public String getSiretNumber() {
        return siretNumber;
    }

    public void setSiretNumber(String siretNumber) {
        this.siretNumber = siretNumber;
    }

    public String getLegalCompanyName() {
        return legalCompanyName;
    }

    public void setLegalCompanyName(String legalCompanyName) {
        this.legalCompanyName = legalCompanyName;
    }

    public Long getTurnOver() {
        return turnOver;
    }

    public void setTurnOver(Long turnOver) {
        this.turnOver = turnOver;
    }
}
