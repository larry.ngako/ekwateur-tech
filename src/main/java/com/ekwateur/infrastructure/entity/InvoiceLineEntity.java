package com.ekwateur.infrastructure.entity;

import com.ekwateur.domain.data.EnergyType;
import javax.persistence.*;

import java.math.BigDecimal;


@Entity(name = "invoice_line")
public class InvoiceLineEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private EnergyType energyType;
    private int quantity;
    private BigDecimal amount;

    @Column( nullable = false, unique = true)
    private String invoiceHeaderReference;

    public InvoiceLineEntity(Long id, EnergyType energyType, int quantity, BigDecimal amount, String invoiceHeaderReference) {
        this.id = id;
        this.energyType = energyType;
        this.quantity = quantity;
        this.amount = amount;
        this.invoiceHeaderReference = invoiceHeaderReference;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public void setEnergyType(EnergyType energyType) {
        this.energyType = energyType;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getInvoiceHeaderReference() {
        return invoiceHeaderReference;
    }

    public void setInvoiceHeaderReference(String invoiceHeaderReference) {
        this.invoiceHeaderReference = invoiceHeaderReference;
    }
}
